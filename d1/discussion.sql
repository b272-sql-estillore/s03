[SECTION] Inserting Records

INSERT INTO artists (name) VALUES ("Rivermaya");
INSERT INTO artists (name) VALUES ("Psy");

-- inserting data to a particular table with 2 or more columns
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Psy 6", "2012-1-1", 2);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Trip", "1996-1-1", 1);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Gangnam Style", 253, "K-pop", 3);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Ulan", 234, "OPM", 4);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("214", 254, "OPM", 4);

[SECTION] -- READ AND SELECT RECORDS/DATA
-- Display the title and genre of all the songs
SELECT song_name, genre FROM songs;

-- Display the song name of all the OPM songs
SELECT song_name FROM songs WHERE genre = "OPM";

SELECT * FROM songs;

-- Display the title and length of the OPM songs that are more than 2 mins

SELECT song_name, length FROM songs WHERE length>200 AND genre="OPM";

SELECT song_name, length FROM songs WHERE length>230 AND genre="OPM";

[SECTION] Updating records
UPDATE songs SET length = 259 where song_name = "214";

UPDATE songs SET song_name = "Kundiman Updated" where song_name = "Ulan";

-- [SECTION] Delete Record

DELETE FROM songs WHERE genre = "OPM" AND length >250;

DELETE * FROM songs WHERE genre="K-pop";